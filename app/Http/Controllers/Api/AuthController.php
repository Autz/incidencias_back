<?php
/**
 * @author Marco AT <marco.at720@gmail.com>
 */
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\JWTAuth;

class AuthController extends Controller
{
    private $jwt;
    
    public function __construct(JWTAuth $jwt){
        $this->jwt = $jwt;
    }

    public function login(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = $this->jwt->attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token'));
    }
    /**
     * @desc get the current token and invalidate them
     * @return JSON ['logout']
     */
    public function logout(){
        $token = $this->jwt->getToken();
        $this->jwt->invalidate($token);

        return response()->json(['logout']);
    }
    public function getAuthenticatedUser()
    {
        try {

            if (! $user = $this->jwt->parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['error'=>'token_absent'], $e->getStatusCode());

        }

        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }
}
