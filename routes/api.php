<?php

use Illuminate\Http\Request;

/*
Ejemplo de rutas para ingresar y salir:
Route::post('auth/login', 'Api\AuthController@login');
Route::post('auth/logout', 'Api\AuthController@logout');

Ejemplo de ruta para acceder a los datos del usuario actual:
Route::get('auth/current','Api\AuthController@getAuthenticatedUser');

ejemplo de rutas de la api protegidas por un middleware
Route::group(['middleware' => ['jwt.auth',] ], function () {
    
    Route::resource('task', 'TaskController',
    ['only'=>['index','store', 'update', 'destroy', 'show']]);
    
});
*/