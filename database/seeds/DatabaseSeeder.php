<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Task;


//NOTA IMPORTANTE SI TE SALE ERROR USA "composer dump-autoload"


class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
    }
}
